/// <reference types="node" />
interface Options {
    loadContent: boolean;
    calculateSize: boolean;
    contentInJson: boolean;
}
declare class TreeElement {
    name: string;
    parent: TreeFolder;
    children: {
        [name: string]: TreeFile | TreeFolder;
    };
    constructor();
    fullPath(): string;
    realtivePath(): string;
    goto(relativePath: string): TreeFile | TreeFolder;
    toJSON(): any;
}
export declare class TreeFolder extends TreeElement {
    name: string;
    parent: TreeFolder;
    constructor(name: string, parent: TreeFolder);
}
export declare class TreeFile extends TreeElement {
    readonly fileName: string;
    extension: string;
    size: number;
    content: Buffer;
    options: Options;
    realtivePath(): string;
    fullPath(): string;
    getContent(): Promise<Buffer>;
    getSize(): Promise<number>;
    toJSON(): {
        extension: string;
        name: string;
        size: number;
        content: string;
    };
}
declare const parseDirectory: (absolutePath: string, options?: Options) => Promise<TreeFolder>;
export default parseDirectory;
//# sourceMappingURL=index.d.ts.map