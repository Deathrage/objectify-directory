export const json = (json: string) => {
    return JSON.parse(json);
}

export const result = (res: any) => {
    return JSON.stringify(res, (key, value): any => { return key === 'parent' ? undefined : value})
}