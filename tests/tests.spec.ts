import parseDirectory, { TreeFile } from "../index";
import * as path from "path";
import { json, result } from "./helpers";

describe('Tests for one folder with one file', () => {

    let dirMapProm = parseDirectory(path.resolve(__dirname, 'structure/anotherfolder/outerfolder/innerfolder'));

    test('Map folder with one file', async () => {
        let prase = result(await dirMapProm);
        expect(json(prase)).toMatchObject(json('{"innerfolder": {"pseudofile.docx": {"extension": "docx", "name": "pseudofile"}}, "root": "D:\\\\Repos\\\\git\\\\directory-to-object\\\\tests\\\\structure\\\\anotherfolder\\\\outerfolder"}'));
    });

    test('Goto file', async () => {
        let file = result((await dirMapProm).goto('pseudofile.docx'));
        expect(json(file)).toMatchObject(json('{"extension": "docx", "name": "pseudofile"}'));
    });

    test('Goto file and back', async () => {
        let file = result((await dirMapProm).goto('pseudofile.docx').goto('..'));
        expect(json(file)).toMatchObject(json('{"innerfolder": {"pseudofile.docx": {"extension": "docx", "name": "pseudofile"}}, "root": "D:\\\\Repos\\\\git\\\\directory-to-object\\\\tests\\\\structure\\\\anotherfolder\\\\outerfolder"}'));
    });

    test('Move to file and check size', async () => {
        let file = (await dirMapProm).goto('pseudofile.docx') as TreeFile;
        let size = await file.getSize();
        expect(size).toEqual(0);
    });

    test('Get full path to file', async () => {
        let file = (await dirMapProm).goto('pseudofile.docx') as TreeFile;
        let path = file.fullPath();
        expect(path).toEqual('D:\\Repos\\git\\directory-to-object\\tests\\structure\\anotherfolder\\outerfolder\\innerfolder\\pseudofile.docx')
    });
});

describe('Tests complex dir structure', () => {
    let dirMapProm = parseDirectory(path.resolve(__dirname, 'structure'));

    test('Map complex structure', async () => {
        let map = result(await dirMapProm);
        expect(json(map)).toMatchObject(json('{"root": "D:\\\\Repos\\\\git\\\\directory-to-object\\\\tests", "structure": {"anotehrfile.js": {"extension": "js", "name": "anotehrfile"}, "anotherfolder": {"mightfile.cpp": {"extension": "cpp", "name": "mightfile"}, "outerfolder": {"innerfolder": {"pseudofile.docx": {"extension": "docx", "name": "pseudofile"}}, "magicwand.pptx": {"extension": "pptx", "name": "magicwand"}}}, "file.js": {"extension": "js", "name": "file"}, "folder": {"badfile.js": {"extension": "js", "name": "badfile"}, "goodfile.js": {"extension": "js", "name": "goodfile"}, "randomtext.txt": {"extension": "txt", "name": "randomtext"}}}}'));
    });
});