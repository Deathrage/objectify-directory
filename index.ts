import * as path from 'path';
import * as fs from 'fs';
import { promisify } from 'util';

interface Options {
	loadContent: boolean,
    calculateSize: boolean,
    contentInJson: boolean;
}

const normalize = (relPath: string) => {
    return (relPath.replace(/^\.\//, '') || '.');
}

class TreeElement {
    public name: string
    public parent: TreeFolder
    public children: { [name: string]: TreeFile|TreeFolder };

    constructor() {
        this.children = {};
    }

    // common methods
    fullPath(): string {
        return this.parent.fullPath() + path.sep + this.name;
    }

    realtivePath(): string {
        return this.parent.realtivePath() + path.sep + this.name;
    }

    goto(relativePath: string): TreeFile|TreeFolder {
        let normalPath = normalize(relativePath);

        // give root
        if (normalPath === '/') {
            if (this.parent) return this.parent;
            else return this;
        }

        // dot means I want myself
        if (/^\.\/?$/.test(normalPath)) return this;

        // go to parent
        else if (/^\.\.\/?/.test(normalPath)) {
            let newPath = normalPath.replace(/^\.\.\/?/, './');

            if (this.parent) return this.parent.goto(newPath);
            else return null;
        }

        // go to child
        else if (/^(\.\/)?.+/.test(normalPath)) {
            let match = (normalPath.match(/^(\.\/)?.+/)[0] || '').replace('./', '').split('/').shift();
            let newPath = normalPath.replace(new RegExp(match + '\/?'), './');

            if (this.children[match]) return this.children[match].goto(newPath);
            else return null;
        } else {
            throw new Error('Unrecognized path string');
        }
    }

    toJSON(): any {
        return this.children;
    }
}

export class TreeFolder extends TreeElement {

    constructor(public name: string, public parent: TreeFolder) {
        super();
    }
}

export class TreeFile extends TreeElement {

    get fileName() {
        return this.name + '.' + this.extension;
    }

    extension: string;
    size: number;
    content: Buffer;
    options: Options;

    // override of inhereted methods
    realtivePath(): string {
        return this.parent.realtivePath() + path.sep + this.fileName;
    }

    fullPath(): string {
        return this.parent.fullPath() + path.sep + this.fileName;
    }

    // filespecific methods
    async getContent() {
        if (this.content) return this.content;
        return await readFile(this.fullPath());
    }
    async getSize() {
        if (this.size) return this.size;
        return (await this.getContent()).length;
    }

    toJSON() {
        let repre =  {
            extension: this.extension,
            name: this.name,
            size: this.size,
            content: (undefined as string)
        };
        if (this.options) {
            if (this.options.contentInJson && this.content) {
                repre.content = this.content.toString();
            }
        }
        return repre;
    }
}

class RootTreeFolder extends TreeFolder {

    root: string;

    constructor(root: string) {
        let rootAr = root.split(path.sep);
        super(rootAr.pop(), null);
        this.root = rootAr.join(path.sep);
    }

    realtivePath(): string {
        return path.sep + this.name;
    }

    fullPath() {
        return this.root + path.sep + this.name;
    }

    toJSON() {
        return {
            [this.name]: this.children,
            root: this.root
        };
    }
}

const readdir = promisify(fs.readdir);
const lstat = promisify(fs.lstat);
const readFile = promisify(fs.readFile);

const mapDirectory = (dirPath: string, parent: TreeFolder, options?: Options) => {
    let treeFolder = new TreeFolder(dirPath.split(path.sep).slice(-1)[0], parent);
    return mapDirectoryContent(dirPath, treeFolder, options);
}

const mapDirectoryContent = async (dirPath: string, treeFolder: TreeFolder, options?: Options) => {
    let folderContent = await readdir(dirPath);

    await Promise.all(folderContent.map(async (item) => {

        let newPath = path.resolve(dirPath, item);
        let stats =  await lstat(newPath);

        if (stats.isDirectory()) {
            treeFolder.children[item] = await mapDirectory(newPath, treeFolder, options);
        } else if (stats.isFile()) {
            treeFolder.children[item] = await parseFile(newPath, treeFolder, options);
        } else {
            treeFolder.children[item] = null;
        }
    }));

    return treeFolder;
}

const parseFile = async (filePath: string, parent: TreeFolder, options?: Options) => {
    let fileName = filePath.split(path.sep).pop();
    let fileNameArray = fileName.split('.');

    let treeFile = new TreeFile();
    treeFile.extension = fileNameArray.pop();
    treeFile.name = fileNameArray.join('.');
    treeFile.parent = parent;
    treeFile.options = options;

    if (options instanceof Object) {

        let content = options.loadContent ? await readFile(filePath): null;
        if (options.loadContent) {
            treeFile.content = content;
        } 
        if (options.calculateSize) {
            treeFile.size = content ? content.length : (await readFile(filePath)).length;
		}
    }

    return treeFile;
}

const parseDirectory = async (absolutePath: string, options?: Options) => {
    if (typeof absolutePath !== 'string') throw new Error('Path must be string');

    let rootFolder = new RootTreeFolder(absolutePath);

    return await mapDirectoryContent(absolutePath, rootFolder, options);
}

export default parseDirectory;