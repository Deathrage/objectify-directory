# objectify-directory

Maps directory and it's content to a tree of objects with inner navigation.  
**Typed for Typescript**

## Why?

Sometimes it might be usefull to map directory, it's subdirectories and their files to tree of JS objects.

## Getting started

`npm i --save objectify-directory`

```
// ES6
import parseDirectory from 'objectify-directory';
// CommonJS
const parseDirectory = require('objectify-directory').default;

// then
parseDirectory(path.resolve(__dirname,'./folderToMap')).then(root => {

});
// await
let root = parseDirectory(path.resolve(__dirname,'./folderToMap'));

```

When `JSON.stringify()` is called a simplified version of the tree is returned in order to deal with circular references.

### Options

- `loadContent: boolean` loads binary of the file as property of TreeFile object
- `calculateSize: boolean` calculates byte size of the file as property of TreeFile object
- `contentInJson: boolean` includes `Buffer.toString()` in content of TreeFile when `JSON.strigify()` is called, `loadContent` needs to be set `true`

### Types

#### TreeElement

Common ancestor to TreeFolder and TreeFile. Holds methods available to both types.

**Properties**
- `name: string` name of the directory of file (without extension)
- `parent: TreeElement` reference to parent directory (always directory as only dirs can have children)
- `children: Map<string, TreeElement>` collection of children elements, should be accessed only via `.goto` method

**Methods**
- `fullPath(): string` returns full path to the element from the root of the drive
- `relativePath(): string` return relative path from root
- `goto(path: string): TreeElement` navigates through the tree, accepts common path pattern (eg: "../siblingFolder", "./childFolder", "./child.file")

#### TreeFolder

Object representation of folder.

#### TreeFile

Object representation of file

**Properties**
- `extension: string` extension of the file
- `size: number` byte size of the file if option set true
- `content: Buffer` content of the file if option set true

**Methods**
- `getSize(): number` calculates the byte size of the file or returns size proeprty if set
- `getContent(): Buffer` reads content of the file, or return content property if set
